﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Beemove2 : MonoBehaviour {

	public float speed = 8.0f;        // metres per second
	public float turnSpeed = 180.0f;  // degrees per second
	public Transform target;
	public Transform target2;
	public Vector2 heading = Vector3.right; 


	// Use this for initialization
	void Start () {
		
	}
		
	// Update is called once per frame
	void Update () {
		Vector2 direction = target.position - transform.position;
		Vector2 direction2 = target2.position - transform.position;

		// calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;

		if (direction.magnitude < direction2.magnitude) {
			// turn left or right
			if (direction.IsOnLeft(heading)) {
				// target on left, rotate anticlockwise
				heading = heading.Rotate(angle);
			}
			else {
				// target on right, rotate clockwise
				heading = heading.Rotate(-angle);
			}

			transform.Translate(heading * speed * Time.deltaTime);

		}
		else {
			// turn left or right
			if (direction2.IsOnLeft(heading)) {
				// target on left, rotate anticlockwise
				heading = heading.Rotate(angle);
			}
			else {
				// target on right, rotate clockwise
				heading = heading.Rotate(-angle);
			}

			transform.Translate(heading * speed * Time.deltaTime);


		}


	}
		 

	void OnDrawGizmos() {
		// draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);

		// draw target vector in yellow
		Gizmos.color = Color.yellow;
		Vector2 direction = target.position - transform.position;
		Gizmos.DrawRay(transform.position, direction);
	}




}

